# https://docs.docker.com/engine/reference/builder/

FROM scratch AS localfonts
COPY fonts fonts

FROM alpine:3.18.4 AS mscorefonts
RUN apk update -q --no-cache
RUN apk add -q --no-cache --upgrade msttcorefonts-installer
RUN update-ms-fonts --quiet

FROM alpine:3.18.4 AS googlefonts
WORKDIR /work

COPY googlefonts.list googlefonts.list

RUN apk update -q --no-cache
RUN apk add -q --no-cache --update curl zip
RUN \
cat googlefonts.list | sed 's/\r$//' | xargs -r -I {} sh -c ' \
export FONTNAME="{}" && \
export PARTURL=$(echo "$FONTNAME" | tr " " "+") && \
curl https://fonts.google.com/download?family=$PARTURL -o "/tmp/$FONTNAME.zip" && \
mkdir -p "/fonts/truetype/$FONTNAME" && \
unzip -o "/tmp/$FONTNAME.zip" -d "/fonts/truetype/$FONTNAME" \
'

FROM pandoc/latex:2.16.2 AS pandoc
ENTRYPOINT [ "/bin/sh", "-c" ]
WORKDIR /work

ARG FONTSDIR=/usr/share/fonts

COPY --from=localfonts /fonts "$FONTSDIR"
COPY --from=mscorefonts /usr/share/fonts "$FONTSDIR"
COPY --from=googlefonts /fonts "$FONTSDIR"

RUN fc-cache -f && rm -rf /var/cache/*
