# pandoc-latex-fonts

Docker образ с Pandoc, Latex и набором шрифтов.

За основу взять образ [`pandoc/latex`](https://hub.docker.com/r/pandoc/latex).

## Build

Сборка образа и запуск контейнера `pandoc-latex-fonts`:

```shell
docker build . -f Dockerfile -t pandoc-latex-fonts
docker run --rm -i -t --name pandoc-latex-fonts pandoc-latex-fonts /bin/sh
```

Список установленных шрифтов:

```shell
fc-list : family | sort | uniq
```

Версия `pandoc`:

```shell
pandoc -v
```

## Fonts

### Local fonts

Шрифты из директории [`fonts`](/fonts)

### Microsoft fonts

Шрифты из пакета [`msttcorefonts-installer`](https://pkgs.alpinelinux.org/package/edge/community/x86_64/msttcorefonts-installer)

### Google fonts

https://fonts.google.com/

Список устанавливаемых Google шрифтов [`googlefonts.list`](googlefonts.list)
